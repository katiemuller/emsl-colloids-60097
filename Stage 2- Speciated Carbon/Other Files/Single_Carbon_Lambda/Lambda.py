import sys
import os

try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import pflotran as pft

path = []
path.append('.')

filename = 'single_carbon_lambda-obs-0.pft'

f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Carbon Speciation",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(9)

C_tot = []
C_sum = 0
C_sum2 = []

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
  C_tot.append(data.get_array('y'))

plt.legend(loc=1,title='Time [s]')

plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)
"""
#### Total Carbon Plot ######
#####Calculating Total Carbon at each Timestep. Stored in C_sum2
for time in range(len(data.get_array('x'))):
   C_sum = 0 
   for i in range(len(icol)):
   	C_sum = C_sum + C_tot[i][time]
   C_sum2.append(C_sum)

f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Total Carbon",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

plt.plot(data.get_array('x'),C_sum2[:],label='C_total')
"""

#### Aqueous Speciation Plot ####
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Aq. Speciation",fontsize=16)
plt.xlabel('Time [min]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(2)
icol.append(3)
icol.append(4)
icol.append(5)
icol.append(6)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))

plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

##### Oxygen Plot ######
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Oxygen Consumption",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(7)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
 

plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

###### Biomass Plot ######
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Biomass",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(8)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))

plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)
#plt.show()

###### CO2 Plot ######
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("CO2 Generation",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(10)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))

plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)
plt.show()
