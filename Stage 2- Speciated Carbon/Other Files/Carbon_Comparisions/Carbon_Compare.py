import sys
import os

# try:
#   pflotran_dir = os.environ['PFLOTRAN_DIR']
# except KeyError:
#   print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
#   sys.exit(1)
# sys.path.append(pflotran_dir + '/src/python')
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
#import pflotran as pft
import csv
import pandas as pd


path = []
path.append('.')

#1 is speciated carbon (Note: Changed files to csv files)
#2 is single carbon    (Note: Changed files to csv files)
filename1 = 'speciated_carbon_lambda-obs-0.csv'
filename2 = 'single_carbon_lambda-obs-0.csv'

df = pd.read_csv(filename1, sep = " " or ",")
print(df['Time [d]'])


"""
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Single Carbon",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

#1 is speciated carbon
icol1 = []
icol1.append(9)

C_tot = []
C_sum = 0
C_sum2 = []

#2 is single carbon
icol2 = []
icol2.append(9)


#### Total Carbon Plot ######
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Total Carbon",fontsize=16)
plt.xlabel('Time [day]')
plt.ylabel('Concentration [M]')

##### Calculating Total Carbon at each Timestep. Stored in C_sum2 ######
for i in range(len(icol1)):

  #data1 = pft.Dataset(filename1,1,icol1[i])
  C_tot.append(data1.get_array('y'))

for time in range(len(data1.get_array('x'))):
   C_sum = 0
   for i in range(len(icol1)):
   	C_sum = C_sum + C_tot[i][time]
   C_sum2.append(C_sum)

plt.plot(data1.get_array('x'),C_sum2[:],label = "Total Carbon (speciced)")

##### Single carbon plot ###
for i in range(len(icol2)):
 # data2 = pft.Dataset(filename2,1,icol2[i])
  plt.plot(data2.get_array('x'),data2.get_array('y'),label=data2.get_name('yname'))

plt.legend(loc=1,title='Time [day]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,

                  bottom=.12,top=.9,
                  left=.12,right=.9)
plt.show()


#### Aqueous Speciation Plot ####
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Aq. Speciation",fontsize=16)
plt.xlabel('Time [day]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(2)
icol.append(3)
icol.append(4)
icol.append(5)
icol.append(6)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))

plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

##### Oxygen Plot ######
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Oxygen Consumption",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(7)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))


plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

###### Biomass Plot ######
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Biomass",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(8)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))

plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)
#plt.show()

###### CO2 Plot ######
f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("CO2 Generation",fontsize=16)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [M]')

icol = []
icol.append(10)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))

plt.legend(loc=1,title='Time [s]')
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)
plt.show()
"""
