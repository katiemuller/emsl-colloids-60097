#!/usr/bin/env python3
import numpy as np

f =open('WC_EXT_1_t0_L_r1_stoichMet_O2_from_lambda_bins.csv','r')
f2 = open('reaction_network.txt','w')

header = f.readline()
species = header.strip().split(',')
carbon_species = []
species = np.array(species)
species = np.where(species == 'hco3' , 'HCO3-', species)
species = np.where(species == 'nh4' , 'NH4+', species)
species = np.where(species == 'hpo4' , 'HPO4--', species)
species = np.where(species == 'hs' , 'HS-', species)
species = np.where(species == 'h' , 'H+', species)
species = np.where(species == 'acceptor', 'O2', species)
species = np.where(species == 'biom' , 'BIOMASS', species)


while True:
    string = f.readline()
    if len(string) == 0:
        break
    w = string.strip().split(',')
    for i in range(1,len(w)):
        if i == 1:
            f2.write('<=> {} {} '.format(w[i],w[0].split('.')[0]))
	elif i == list(species).index('h2o') or i == list(species).index('e'):
	    continue
        else:
            f2.write('+ {} {} '.format(w[i],species[i]))
	    out = ('{} {}'.format(w[i], species[i]))

    f2.write('\n')
    carbon_species.append(w[0].split('.')[0])
f2.close()
f.close()

f = open('primary_species_block.txt','w')
f.write('  PRIMARY_SPECIES\n')

for i in range(2,len(w)):
   if i == list(species).index('h2o') or i == list(species).index('e'):
	continue
   else:
    	f.write('    {}\n'.format(species[i]))

for w in carbon_species:
    f.write('    {}\n'.format(w))
f.write('  /\n')
f.close()
