#Description: Colloids EMSL Project

# Batch Reactor. Simple organic matter oxidation
# Assumptions:
# Using generic carbon source (CH2O)
# AquaMEND expression for aerobic respiration


# Reactions included:
# (1) Aerobic respiration
#      12.2 CH2O + 7.2 O2(aq) + NH4+ -> C5H7O2N + 7.2 HCO3- + 3 H2O + 8.2 H+

# Species included:
# CH2O = general organic matter, assumed to be CH2O for stoich.
# C5H7O2N = general biomass

#=========================== simulation ================================
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      MODE GIRT
    /
  /
END

SUBSURFACE

#=========================== numerical methods ================================
NUMERICAL_METHODS TRANSPORT

  NEWTON_SOLVER
    # Uncomment to debug reactions
    #NUMERICAL_JACOBIAN
  /

  LINEAR_SOLVER
    SOLVER DIRECT
  /

END

#=========================== regression =======================================
REGRESSION
  CELL_IDS
    1
  /
END

#=========================== flow mode ========================================
# Uniform velocity (see below). No flow mode specified.

#=========================== useful tranport parameters ==================
SPECIFIED_VELOCITY
  UNIFORM? YES
  DATASET 0.d0 0.d0 0.d0
END

#=========================== chemistry ========================================
CHEMISTRY
  PRIMARY_SPECIES
    CH2O
    O2(aq)
    NH4+
    C5H7O2N
    HCO3-
    H+
#    CO2(aq)
  /
  SECONDARY_SPECIES
    OH-
    CO3--
    CO2(aq)
    NH3(aq)
  /

  MICROBIAL_REACTION
    CONCENTRATION_UNITS ACTIVITY

# Aerobic Respiration (Expression from AquaMEND/MEND)
    REACTION 12.2 CH2O + 7.2 O2(aq) + NH4+ <-> C5H7O2N + 7.2 HCO3- + 8.2 H+
 
    RATE_CONSTANT 1.5d-10
    MONOD
      SPECIES_NAME CH2O
      HALF_SATURATION_CONSTANT 1.d-5        ! A is the donor
#      THRESHOLD_CONCENTRATION 1.d-20
    /
    MONOD
      SPECIES_NAME O2(aq)
      HALF_SATURATION_CONSTANT 1.d-5        ! B is the acceptor
#      THRESHOLD_CONCENTRATION 1.d-11
    /

#    INHIBITION
#      SPECIES_NAME C(aq)
#      TYPE INVERSE_MONOD
#      INHIBITION_CONSTANT 6.d-4   ! C is the product and inhibits when too high
#    /
#    BIOMASS
#      SPECIES_NAME D(im)
#      YIELD 0.01d0
#    /
  /
#  IMMOBILE_DECAY_REACTION
#    SPECIES_NAME D(im)
#    RATE_CONSTANT 1.d-9
#  /
#  DATABASE ../../../database/hanford.dat
#  DATABASE database/cybernetic.dat
  DATABASE ../../database/hanford.dat
  LOG_FORMULATION
  ACTIVITY_COEFFICIENTS OFF
  OUTPUT
    ALL
    TOTAL
    CO2(aq)
    pH
  /
END

#=========================== solver options ===================================

#=========================== discretization ===================================
GRID
  TYPE STRUCTURED
  NXYZ 1 1 1
  BOUNDS
    0.d0 0.d0 0.d0
    1.d0 1.d0 1.d0
  /
END

#=========================== fluid properties =================================
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END

#=========================== material properties ==============================
MATERIAL_PROPERTY soil1
  ID 1
  POROSITY 0.25d0
  TORTUOSITY 1.d0
END

#=========================== output options ===================================
OUTPUT
#  TIMES y 5. 10. 15. 20.
#skip
#  FORMAT TECPLOT POINT
  PERIODIC_OBSERVATION TIMESTEP 1
#noskip
END

#=========================== times ============================================
TIME
  FINAL_TIME 25.d0 d            
  INITIAL_TIMESTEP_SIZE 1.d0 h
  MAXIMUM_TIMESTEP_SIZE 1.d0 h
#  MAXIMUM_TIMESTEP_SIZE 1.d-3 y
END

#=========================== regions ==========================================
REGION all
  COORDINATES
    -1.d20 -1.d20 -1.d20
    1.d20 1.d20 1.d20
  /
END

REGION pt
  COORDINATE 0.5d0 0.5d0 0.5d0
END

#=========================== observation points ===============================
OBSERVATION
  REGION pt
END

#=========================== transport conditions =============================
TRANSPORT_CONDITION initial
  TYPE ZERO_GRADIENT
  CONSTRAINT_LIST
    0.d0 initial
  /
END

#=========================== constraints ======================================
CONSTRAINT initial
  CONCENTRATIONS
    CH2O     2.1d-4  T
    O2(aq)   4.06d-3 T
    NH4+     1.01d-4  T
    C5H7O2N  1.d-5  T
    HCO3-    9.37d-5  T
    H+       6.94   P
  /
#  IMMOBILE
#    D(im) 1.d-7
#  /
END

#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  TRANSPORT_CONDITION initial
  REGION all
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL soil1
END

END_SUBSURFACE
