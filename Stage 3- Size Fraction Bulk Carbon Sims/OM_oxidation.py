import sys
import os
try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import pflotran as pft

path = []
path.append('.')

#filename = './cybernetic_analytical-obs-0.tec'
#filename = './cybernetic-obs-0.tec'
#filename = 'reaction_sandbox_Ncycle_batch-obs-0.pft'
#filename = 'debug-obs-0.pft'
#filename = './OM_oxidation-obs-0.pft'
filename = './OM_oxidation_AquaMEND-obs-0.pft'

f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("OM oxidation",fontsize=16)
plt.xlabel('Time [day]')
plt.ylabel('Concentration [nM]')

plt.xlim(0.,25)
#plt.ylim(4.8,8.2)
#plt.yscale('log')
#plt.grid(True)

icol = []
icol.append(2)
icol.append(3)
icol.append(4)
icol.append(5)
icol.append(6)
icol.append(7)
#icol.append(8)
#icol.append(9)

for i in range(len(icol)):
  data = pft.Dataset(filename,1,icol[i])
#converting data for plotting purposes from sec to min and M to nM.
  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))

#Time in days
  #plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))


#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
plt.legend(loc=1,title='Time [d]')
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
#plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)
plt.show()
