#Description: Colloids EMSL Project

# Batch Reactor. Simple organic matter oxidation
# Assumptions:
# Using generic carbon source (CH2O)
# AquaMEND expression for aerobic respiration

# Conceputal Model: Each size fraction (S, M, L) has its own release rate from soil (k, release)
#     and respiration constants (umax, VhO2, Vh,CH2O).
#     CO2 produced (in the form of HCO3-) gets summed to compare to bulk incubation CO2 generation curves. 

# Reactions included:
# (1) Aerobic respiration
#      12.2 CH2O + 7.2 O2(aq) + NH4+ -> C5H7O2N + 7.2 HCO3- + 3 H2O + 8.2 H+

# Species included:
# CH2O,S = Small size fracition, general organic matter, assumed to be CH2O for stoich.
# CH2O,M = Small size fracition, general organic matter, assumed to be CH2O for stoich.
# CH2O,L = Small size fracition, general organic matter, assumed to be CH2O for stoich.
# C5H7O2N = general biomass

#=========================== simulation ================================
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      MODE GIRT
    /
  /
END

SUBSURFACE

#=========================== numerical methods ================================
NUMERICAL_METHODS TRANSPORT

  NEWTON_SOLVER
  /

  LINEAR_SOLVER
    SOLVER DIRECT
  /

END

#=========================== regression =======================================
REGRESSION
  CELL_IDS
    1
  /
END

#=========================== flow mode ========================================
# Uniform velocity (see below). No flow mode specified.

#=========================== useful tranport parameters ==================
SPECIFIED_VELOCITY
  UNIFORM? YES
  DATASET 0.d0 0.d0 0.d0
END

#=========================== chemistry ========================================
CHEMISTRY
  PRIMARY_SPECIES
    CH2O_s_(s)
#    CH2O_m_(s)
#    CH2O_l_(s)
    CH2O_s
#    CH2O_m
#    CH2O_l
    O2(aq)
    NH4+
    C5H7O2N
    HCO3-
    H+
  /
  SECONDARY_SPECIES
    OH-
    CO3--
    CO2(aq)
    NH3(aq)
  /

# Release of Carbon from Soil- SMALL
  GENERAL_REACTION
    REACTION CH2O_s_(s) <-> CH2O_s
    FORWARD_RATE 1.d-15 
    BACKWARD_RATE 0.d0
   /
    
#MICROBIAL_REACTION
#    CONCENTRATION_UNITS ACTIVITY    
# Aerobic Respiration 1- SMALL (Expression from AquaMEND/MEND)
#    REACTION 12.2 CH2O_s + 7.2 O2(aq) + NH4+ <-> C5H7O2N + 7.2 HCO3- + 8.2 H+
 
#    RATE_CONSTANT 1.5d-10
#    MONOD
#      SPECIES_NAME CH2O_s
#      HALF_SATURATION_CONSTANT 1.d-5        ! A is the donor
#    /
#    MONOD
#      SPECIES_NAME O2(aq)
#      HALF_SATURATION_CONSTANT 1.d-5        ! B is the acceptor
#    /
#  /  
  DATABASE hanford.dat
  LOG_FORMULATION
  ACTIVITY_COEFFICIENTS OFF
  OUTPUT
    ALL
    TOTAL
    CO2(aq)
    pH
  /
END

#=========================== solver options ===================================

#=========================== discretization ===================================
GRID
  TYPE STRUCTURED
  NXYZ 1 1 1
  BOUNDS
    0.d0 0.d0 0.d0
    1.d0 1.d0 1.d0
  /
END

#=========================== fluid properties =================================
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END

#=========================== material properties ==============================
MATERIAL_PROPERTY soil1
  ID 1
  POROSITY 0.25d0
  TORTUOSITY 1.d0
END

#=========================== output options ===================================
OUTPUT
#  TIMES y 5. 10. 15. 20.
#skip
#  FORMAT TECPLOT POINT
  PERIODIC_OBSERVATION TIMESTEP 1
#noskip
END

#=========================== times ============================================
TIME
  FINAL_TIME 25.d0 d            
  INITIAL_TIMESTEP_SIZE 1.d0 h
  MAXIMUM_TIMESTEP_SIZE 1.d0 h
#  MAXIMUM_TIMESTEP_SIZE 1.d-3 y
END

#=========================== regions ==========================================
REGION all
  COORDINATES
    -1.d20 -1.d20 -1.d20
    1.d20 1.d20 1.d20
  /
END

REGION pt
  COORDINATE 0.5d0 0.5d0 0.5d0
END

#=========================== observation points ===============================
OBSERVATION
  REGION pt
END

#=========================== transport conditions =============================
TRANSPORT_CONDITION initial
  TYPE ZERO_GRADIENT
  CONSTRAINT_LIST
    0.d0 initial
  /
END

#=========================== constraints ======================================
CONSTRAINT initial
  CONCENTRATIONS
    CH2O_s_(s)   1.1d2  T
    CH2O_m_(s)   1.1d2  T
    CH2O_l_(s)   1.1d2  T
    CH2O_s   1.d-3  T
    CH2O_m   5.d-3  T
    CH2O_l   8.d-3  T
    O2(aq)   4.06d-3 T
    NH4+     1.29d-4  T
    C5H7O2N  1.d-20  T
    HCO3-    1.d-20  T
    H+       6.264   P
  /
END

#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  TRANSPORT_CONDITION initial
  REGION all
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL soil1
END

END_SUBSURFACE
